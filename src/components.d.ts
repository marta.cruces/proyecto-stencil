/* eslint-disable */
/* tslint:disable */
/**
 * This is an autogenerated file created by the Stencil compiler.
 * It contains typing information for all components that exist in this project.
 */
import { HTMLStencilElement, JSXBase } from "@stencil/core/internal";
export namespace Components {
    interface AuthComponent {
    }
    interface MyComponent {
        /**
          * The first name
         */
        "first": string;
        /**
          * The last name
         */
        "last": string;
        /**
          * The middle name
         */
        "middle": string;
    }
    interface PostComponent {
        "datePost": string;
        "textPost": string;
        "titlePost": string;
        "usernamePost": string;
    }
    interface SingleComment {
        "commentContent": string;
        "commentNickname": string;
    }
    interface SinglePost {
        "authorPost": string;
        "datePost": string;
        "textPost": string;
        "titlePost": string;
    }
}
declare global {
    interface HTMLAuthComponentElement extends Components.AuthComponent, HTMLStencilElement {
    }
    var HTMLAuthComponentElement: {
        prototype: HTMLAuthComponentElement;
        new (): HTMLAuthComponentElement;
    };
    interface HTMLMyComponentElement extends Components.MyComponent, HTMLStencilElement {
    }
    var HTMLMyComponentElement: {
        prototype: HTMLMyComponentElement;
        new (): HTMLMyComponentElement;
    };
    interface HTMLPostComponentElement extends Components.PostComponent, HTMLStencilElement {
    }
    var HTMLPostComponentElement: {
        prototype: HTMLPostComponentElement;
        new (): HTMLPostComponentElement;
    };
    interface HTMLSingleCommentElement extends Components.SingleComment, HTMLStencilElement {
    }
    var HTMLSingleCommentElement: {
        prototype: HTMLSingleCommentElement;
        new (): HTMLSingleCommentElement;
    };
    interface HTMLSinglePostElement extends Components.SinglePost, HTMLStencilElement {
    }
    var HTMLSinglePostElement: {
        prototype: HTMLSinglePostElement;
        new (): HTMLSinglePostElement;
    };
    interface HTMLElementTagNameMap {
        "auth-component": HTMLAuthComponentElement;
        "my-component": HTMLMyComponentElement;
        "post-component": HTMLPostComponentElement;
        "single-comment": HTMLSingleCommentElement;
        "single-post": HTMLSinglePostElement;
    }
}
declare namespace LocalJSX {
    interface AuthComponent {
    }
    interface MyComponent {
        /**
          * The first name
         */
        "first"?: string;
        /**
          * The last name
         */
        "last"?: string;
        /**
          * The middle name
         */
        "middle"?: string;
    }
    interface PostComponent {
        "datePost"?: string;
        "onSelectPost"?: (event: CustomEvent<any>) => void;
        "textPost"?: string;
        "titlePost"?: string;
        "usernamePost"?: string;
    }
    interface SingleComment {
        "commentContent"?: string;
        "commentNickname"?: string;
    }
    interface SinglePost {
        "authorPost"?: string;
        "datePost"?: string;
        "textPost"?: string;
        "titlePost"?: string;
    }
    interface IntrinsicElements {
        "auth-component": AuthComponent;
        "my-component": MyComponent;
        "post-component": PostComponent;
        "single-comment": SingleComment;
        "single-post": SinglePost;
    }
}
export { LocalJSX as JSX };
declare module "@stencil/core" {
    export namespace JSX {
        interface IntrinsicElements {
            "auth-component": LocalJSX.AuthComponent & JSXBase.HTMLAttributes<HTMLAuthComponentElement>;
            "my-component": LocalJSX.MyComponent & JSXBase.HTMLAttributes<HTMLMyComponentElement>;
            "post-component": LocalJSX.PostComponent & JSXBase.HTMLAttributes<HTMLPostComponentElement>;
            "single-comment": LocalJSX.SingleComment & JSXBase.HTMLAttributes<HTMLSingleCommentElement>;
            "single-post": LocalJSX.SinglePost & JSXBase.HTMLAttributes<HTMLSinglePostElement>;
        }
    }
}
