import { newE2EPage } from '@stencil/core/testing';

describe('single-post', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<single-post></single-post>');

    const element = await page.find('single-post');
    expect(element).toHaveClass('hydrated');
  });
});
