import { newSpecPage } from '@stencil/core/testing';
import { SinglePost } from './single-post';

describe('single-post', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SinglePost],
      html: `<single-post></single-post>`,
    });
    expect(page.root).toEqualHtml(`
      <single-post>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </single-post>
    `);
  });
});
