# single-post



<!-- Auto Generated Below -->


## Properties

| Property     | Attribute     | Description | Type     | Default     |
| ------------ | ------------- | ----------- | -------- | ----------- |
| `authorPost` | `author-post` |             | `string` | `undefined` |
| `datePost`   | `date-post`   |             | `string` | `undefined` |
| `textPost`   | `text-post`   |             | `string` | `undefined` |
| `titlePost`  | `title-post`  |             | `string` | `undefined` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
