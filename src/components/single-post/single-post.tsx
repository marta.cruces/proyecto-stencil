import { Component, ComponentInterface, h, Host, Prop } from '@stencil/core';

@Component({
  tag: 'single-post',
  styleUrl: 'single-post.css',
  shadow: true,
})
export class SinglePost implements ComponentInterface {

  @Prop() titlePost: string;
  @Prop() authorPost: string;
  @Prop() datePost: string;
  @Prop() textPost: string;

  render() {
    return (
    <Host>
      <div class="single-post__header">
        <h3 class="single-post__title">{this.titlePost}</h3>
        <p class="single-post__author">{this.authorPost}<span class="single-post__date"> - {this.datePost}</span></p>
      </div>
      <p class="single-post__text">{this.textPost}</p>
    </Host>
    );
  }

}
