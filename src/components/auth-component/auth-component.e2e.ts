import { newE2EPage } from '@stencil/core/testing';

describe('auth-component', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<auth-component></auth-component>');

    const element = await page.find('auth-component');
    expect(element).toHaveClass('hydrated');
  });
});
