import { newSpecPage } from '@stencil/core/testing';
import { AuthComponent } from './auth-component';

describe('auth-component', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [AuthComponent],
      html: `<auth-component></auth-component>`,
    });
    expect(page.root).toEqualHtml(`
      <auth-component>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </auth-component>
    `);
  });
});
