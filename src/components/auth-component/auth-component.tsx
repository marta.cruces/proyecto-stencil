import { Component, ComponentInterface, h, Host } from '@stencil/core';

@Component({
  tag: 'auth-component',
  styleUrl: 'auth-component.css',
  shadow: true,
})
export class AuthComponent implements ComponentInterface {

  render() {
    return (
      <Host>
        <slot></slot>
      </Host>
    );
  }

}
