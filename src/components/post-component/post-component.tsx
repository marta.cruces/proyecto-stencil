import { Component, ComponentInterface, Event, EventEmitter, h, Host, Prop } from '@stencil/core';

@Component({
  tag: 'post-component',
  styleUrl: 'post-component.css',
  shadow: true,
})
export class PostComponent implements ComponentInterface {

  @Prop() titlePost: string;
  @Prop() usernamePost: string;
  @Prop() datePost: string;
  @Prop() textPost: string;
  @Event() selectPost: EventEmitter;

  onSelect() {
    this.selectPost.emit();
  }

  render() {
    return (
      <Host>
        <article class="post">
          <header class="post__header">
            <h3 class="post__title">{this.titlePost}</h3>
            <p class="post__username">{this.usernamePost}<span class="post__date"> - {this.datePost}</span></p>
          </header>
          <div class="post__content">
            <p class="post__text">{this.textPost}</p>
            <div class="post__footer">
              <button class="post__comments-button" onClick={() => this.onSelect()}>COMMENTS</button>
            </div>
          </div>
        </article>
      </Host>
    );
  }

}
