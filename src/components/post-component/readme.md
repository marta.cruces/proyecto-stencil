# post-component



<!-- Auto Generated Below -->


## Properties

| Property       | Attribute       | Description | Type     | Default     |
| -------------- | --------------- | ----------- | -------- | ----------- |
| `datePost`     | `date-post`     |             | `string` | `undefined` |
| `textPost`     | `text-post`     |             | `string` | `undefined` |
| `titlePost`    | `title-post`    |             | `string` | `undefined` |
| `usernamePost` | `username-post` |             | `string` | `undefined` |


## Events

| Event        | Description | Type               |
| ------------ | ----------- | ------------------ |
| `selectPost` |             | `CustomEvent<any>` |


----------------------------------------------

*Built with [StencilJS](https://stenciljs.com/)*
