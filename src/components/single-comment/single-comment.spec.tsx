import { newSpecPage } from '@stencil/core/testing';
import { SingleComment } from './single-comment';

describe('single-comment', () => {
  it('renders', async () => {
    const page = await newSpecPage({
      components: [SingleComment],
      html: `<single-comment></single-comment>`,
    });
    expect(page.root).toEqualHtml(`
      <single-comment>
        <mock:shadow-root>
          <slot></slot>
        </mock:shadow-root>
      </single-comment>
    `);
  });
});
