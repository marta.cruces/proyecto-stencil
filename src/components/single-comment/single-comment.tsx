import { Component, ComponentInterface, h, Host, Prop } from '@stencil/core';

@Component({
  tag: 'single-comment',
  styleUrl: 'single-comment.css',
  shadow: true,
})
export class SingleComment implements ComponentInterface {
  @Prop() commentNickname: string;
  @Prop() commentContent: string;

  render() {
    return (
      <Host>
        <div class="single-comment__container">
          <div class="single-comment__capital-wrapper"><span class="single-comment__capital-letter">{this.commentNickname[0]}</span></div>
          <div class="single-comment__info">
            <p class="single-comment__nickname">{this.commentNickname}</p>
            <p class="single-comment__content">{this.commentContent}</p>
          </div>
        </div>
      </Host>
    );
  }

}
