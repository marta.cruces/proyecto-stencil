import { newE2EPage } from '@stencil/core/testing';

describe('single-comment', () => {
  it('renders', async () => {
    const page = await newE2EPage();
    await page.setContent('<single-comment></single-comment>');

    const element = await page.find('single-comment');
    expect(element).toHaveClass('hydrated');
  });
});
